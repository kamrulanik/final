<?php

namespace App\Http\Controllers;

use App\Form;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view('create');
    }
    public function create()
    {
        return view('create');
    }

    public function storeme(Request $request)
    {
       $this->validate($request,[
          'name' => 'required',
           'email' => 'required',
           'subject' => 'required',
           'message' => 'required',


       ]);

        $student =new Form;
        $student->name =$request->name;
        $student->email =$request->email;
        $student->subject =$request->subject;
        $student->msg =$request->message;




        $student->save();
        return redirect(route('create'))->with('successMsg','Thank You');
    }


}
