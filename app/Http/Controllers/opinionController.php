<?php

namespace App\Http\Controllers;

use App\Eloquent\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;

class opinionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $r)
    {

        if (Input::has('like_status'))
        {
           $status = Input::get('like_status');
           $selectedStatus = Status::find($status);

           $selectedStatus->likes()->create([
              'user_id' => Auth::user()->id,
              'status_id' =>$status,

           ]);
           return redirect(route('opinion'));
        }

        if (Input::has('post_comment'))
        {
            $status = Input::get('post_comment');
            $commentBox = Input::get('comment-text');
            $selectedStatus = Status::find($status);

            $selectedStatus->comments()->create([
                'comment_text' => $commentBox,
                'user_id' => Auth::user()->id,
                'status_id' => $status
            ]);

            Flash::message('Your comment has been posted');
            return redirect(route('opinion'));
        }

        if (Input::has('status-text'))
        {
            $text = e(Input::get('status-text'));

            $userStatus = new Status();
            $userStatus->status_text = $text;
            $userStatus->users_id = Auth::user()->id;
            $userStatus->save();
            Flash::success('Your status has been posted');
            return redirect(route('opinion'));
        }

        return view('opinion',[

            'top_15_posts' => Status::orderBy('id','DESC')->take(15)->get()
        ]);
    }
    public function index2(){
        return view('qclasseva');
    }

    public function index3(){
        return view('teacher');
    }

    public function index4(){
        return view('result');
    }
}
