<?php

namespace App\Http\Controllers;
use Auth;
use App\Survey;
use App\Answer;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Redirect;
use Illuminate\Database\Eloquent\Model;

class quizmodel extends CI_Model {

  public function getQuestions()
  {
    $this->db->select("quizID, question, choice1, choice2, choice3, choice4, choice5");
    $this->db->from("ouiz");
    
    $query = $this->db->get();
    
    return $query->result();
    
    $num_data_returned = $query->num_rows;
    
    if ($num_data_returned < 1) {
      echo "There is no data in the database";
      exit(); 
    }
  }
}
