<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});
*/



Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/feature', function () {
    return view('feature');
});

Route::get('/example', function () {
    return view('example');
});

Route::get('/service', function () {
    return view('service');
});

Route::get('/design', function () {
    return view('design');
});

Route::get('/form', function () {
    return view('form');
});


Route::get('/quicksurvey', function () {
    return view('quicksurvey');
});

Route::get('/survey/new.php', function () {
    return view('survey.new');
});

Route::get('/basic-grid', function () {
    return view('basic-grid');
});

Route::get('/ulabeva', function () {
    return view('ulabeva');
});

Route::get('/ulab/index.php', function () {
    return view('ulab.index');
});

Route::get('/ulab/qus_show.php', function () {
    return view('ulab.qus_show');
});

Route::get('/ulab/answer.php', function () {
    return view('ulab.answer');
});

Route::get('/quiz_game', function () {
    return view('quiz_game');
});

Route::get('/Questions/quizdisplay', function () {
    return view('quizdisplay');
});


Route::get('/preview', function () {
    return view('preview');
});


Route::get('/teacher', function () {
    return view('teacher');
});


Route::get('/admin2', function () {
    return view('admin');
});
Route::get('/course', function () {
    return view('course');
});

Route::get('/welcome2', function () {
    return view('welcome2');
});


Route::get('/product', function () {
    return view('product');
});


Route::get('/subject', function () {
    return view('subject');
});

Route::get('/patient', function () {
    return view('patient');
});

Route::get('/customer', function () {
    return view('customer');
});















/*Route::get('/sidebar-left', function () {
    return view('sidebar-left');
});

Route::get('/sidebar-right', function () {
    return view('sidebar-right');
});*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/homepage', 'SurveyController@home')->name('homepage');


Route::get('/survey/new', 'SurveyController@new_survey')->name('new.survey');
Route::get('/survey/{survey}', 'SurveyController@detail_survey')->name('detail.survey');
Route::get('/survey/view/{survey}', 'SurveyController@view_survey')->name('view.survey');
Route::get('/survey/answers/{survey}', 'SurveyController@view_survey_answers')->name('view.survey.answers');
Route::get('/survey/{survey}/delete', 'SurveyController@delete_survey')->name('delete.survey');

Route::get('/survey/{survey}/edit', 'SurveyController@edit')->name('edit.survey');
Route::patch('/survey/{survey}/update', 'SurveyController@update')->name('update.survey');

Route::post('/survey/view/{survey}/completed', 'AnswerController@store')->name('complete.survey');
Route::post('/survey/create', 'SurveyController@create')->name('create.survey');

// Questions related
Route::post('/survey/{survey}/questions', 'QuestionController@store')->name('store.question');

Route::get('/question/{question}/edit', 'QuestionController@edit')->name('edit.question');
Route::patch('/question/{question}/update', 'QuestionController@update')->name('update.question');
Route::auth();



Route::any('/opinion', ['as'=>'opinion','uses'=>'opinionController@index']);

Route::get('/qclasseva',['qclasseva', 'uses'=>'opinionController@index2']);

Route::get('/teacher',['teacher', 'uses'=>'opinionController@index3']);
Route::get('/result',['result', 'uses'=>'opinionController@index4']);
Route::post('create','StudentController@store')->name('store');
Route::get('/welcome','StudentController@index')->name('welcome');
Route::get('/teacheradmin','TeacherController@index')->name('teacheradmin');

Route::delete('delete/{id}','StudentController@delete')->name('delete');


Route::get('/create','ContactController@index')->name('home');

Route::get('create5','ContactController@create')->name('create');

Route::post('create5','ContactController@storeme')->name('storeme');





Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
