
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="card">
    <div class="card-header" style="background: #e6e6e6; border-radius: 5px;margin-top: 60px ">{{ $user->name }} - {{$status->created_at->diffForHumans()}}</div>

    <div style="height: 80px; background: #ffffff; border-radius: 5px; text-align: left"  >

        <div class="row">
            <div class="col-md-1">
               <img src="{{$user->getAvatar()}}" class="img-responsive">
            </div>
             <div class="col-md-11">

                 <p style="text-align: left;"> {{ $status->status_text}}</p>
             </div>
            <div class="col-md-12" style="margin-top: 0px">
<hr>
                <ul class="list-unstyled list-inline" style="margin-top: 0px">
                    <li>

                        <button type="button" style="background: #900C3F  " class="btn btn-xs btn-info" data-toggle="collapse" data-target="#view-comments-{{$status->id}}" aria-expanded="false" aria-controls="view-comments-{{$status->id}}"><i class="fa fa-comments-o" style="font-size:14px"></i>View & Comment</button>
                    </li>

                    <li>

                        @if(\App\Eloquent\StatusLikes::where(['status_id' => $status->id, 'user_id' => Auth::user()->id])->first())
@else
                        {!! Form::open() !!}
                        {!! Form::hidden('like_status',$status->id) !!}




                        <button type="submit" style="background: #900C3F  " class="btn btn-xs btn-info"><i class="fa fa-thumbs-up" style="font-size:14px"></i>Like</button>
                        {!! Form::close() !!}
                            @endif
                    </li>
                    <li>
                        {{ $comment_count }} <i class="fa fa-comments-o" style="font-size:14px"></i>

                    </li>
                    <li >
                        {{$like_count }} <i class="fa fa-thumbs-up" style="font-size:14px"></i>
                    </li>
                    <li>
                        <button type="submit" style="background: #900C3F  " class="btn btn-xs btn-info"><i class="fa fa-thumbs-down" style="font-size: 14px"></i>DisLike</button>
                    </li>

                    <li style="float: right">
                        <button type="submit" style="background:  #900C3F;font-size: 14px" class="btn btn-xs btn-info"><i class="fa fa-star" style="font-size: 10px"></i>1</button>
                    </li>
                    <li style="float: right">
                        <button type="submit" style="background: #900C3F;font-size: 14px" class="btn btn-xs btn-info"><i class="fa fa-star" style="font-size: 10px"></i>2</button>
                    </li>

                    <li style="float: right">
                        <button style="background: #900C3F;font-size: 14px" class="btn btn-xs btn-info"><i class="fa fa-star" style="font-size: 10px"></i>3</button>
                    </li>

                    <li style="float: right">
                        <button type="submit" style="background:  #900C3F;font-size: 14px" class="btn btn-xs btn-info"><i class="fa fa-star" style="font-size: 10px"></i>4</button>
                    </li>

                    <li style="float: right">
                        <button type="submit" style="background: #900C3F;font-size: 14px" class="btn btn-xs btn-info"><i class="fa fa-star" style="font-size: 10px"></i>5</button>
                    </li>



                </ul>

            </div>
<br>

        </div>


    </div>

    <div class="panel-footer clearfix">

        {!! Form::open() !!}
        {!! Form::hidden('post_comment',$status->id) !!}
        <div class="form-group">

            <div class="input-group">
                <input required type="text" class="form-control" name="comment-text" id="comment-text" placeholder="Post & Comment">
                <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="fa fa-send"></i>Post</button>
            </span>
            </div>


        </div>




        {!! Form::close() !!}


        <div class="collapse" id="view-comments-{{$status->id}}">

            @if($comments->first())
                @foreach($comments as $comment)

                    <blockquote style="font-size: 14px">
                       <div class="row">
                           <div class="col-md-1">
                                 <img src="{{\App\Eloquent\User::find($comment->user_id)->getAvatar()}}" class="img-responsive">
                           </div>
                           <div class="col-md-11">
                               <ul class="list-inline list-unstyled">
                                  <li>

                                      <a style="color: #900C3F;font-weight: bold" href="">
                                          {{\App\Eloquent\User::find($comment->user_id)->name}}
                                      </a>
                                  </li>
                                   <li>
                                       Commented {{$comment->created_at->diffForHumans()}}


                                   </li>

                               </ul>

                                <p style="margin-top: 10px; background: #ffffff; height: 30px; border-radius: 5px">{{$comment->comment_text}}</p>
                           </div>

                       </div>


                    </blockquote>
                @endforeach






                @else
                <p>This status contains no comment.</p>

             @endif

            </div>

    </div>
</div>