<!DOCTYPE html>
<html lang="">
<head>
<title>SurveyFun</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="fl_left">
      <ul>
        <li><i class="fa fa-phone"></i> contact number</li>
        <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
      </ul>
    </div>
    <div class="fl_right">
      <ul>
        <li><a href="login"><i class="fa fa-lg fa-home"></i></a></li>
        <li><a href="login">Login</a></li>
        <li><a href="register">Register</a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
      <h1><a href="/">SurveyFun</a></h1>
      <p>Survey making website</p>
    </div>
    <div id="quickinfo" class="fl_right">
      <ul class="nospace inline">
        <li><strong>Help line:</strong><br>
          0000000000</li>
        <li><strong>Send mail:</strong><br>
          SurveyFun@gmail.com</li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </header>
  <nav id="mainav" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul class="clear">
      <li class="active"><a href="/">Home</a></li>

      <li><a href="about">About</a></li>
      <li><a href="feature">Features</a></li>
      <li><a href="register">Registration</a></li>
      <li><a href="example">Examples</a></li>
      <li><a href="service">Services</a></li>
      <li><a href="#">Help</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </nav>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/001.jpg');">
  <section id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
  
    <h6 class="heading">Services of SurveyFun</h6>
    <!-- ################################################################################################ -->
  </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="sidebar one_quarter first"> 
      <!-- ################################################################################################ -->
      <h6>About</h6>
      <nav class="sdb_holder">
        <ul>
          <li><a href="#">Solutions</a> </li>
          
            <ul>
              <li><a href="#">Education</a>
              <li><a href="#">Government</a></li>
              <li><a href="#">Business</a></li>
              <li><a href="#">Medical</a></li>
              <li><a href="#">Non Profits</a></li>
            </ul>
         

          <li><a href="#">Features</a> </li>
          
            <ul>
              <li><a href="#">Custom Designs</a></li>
              <li><a href="#">Easy to Use</a></li>
              <li><a href="#">Question Types</a></li>
              <li><a href="#">Execute</a></li>
              </ul>
         
                  
          <li><a href="#">Terms of Use</a></li>

          <li><a href="#">Privacy Policy</a></li>

          <li><a href="#">Contact</a></li>
        </ul>
      </nav>
       </div>
      
      <!-- ################################################################################################ -->
   
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="content three_quarter"> 
      <!-- ################################################################################################ -->
      <div class="sectiontitle">
      <h1 class="heading">Survey Features</h1>

      <div class="boxed">  
        <p>In this website there are many more services which helps a person to make a survey in a easy way.</p>
      </div>
    </div>

    <p><b>Create Survey by Drag and Drop</b></p>

    <p>The third component of the website will be the survey design interface which will allow the registered users to design a survey as per their requirements. All contents i.e. question types, breaks, labels etc. will be shown in the sidebar from where the users can drag and drop those in the design workspace as required.<a href="#">Learn more</a></p>


    <p><b>Knowledge</b></p>

    <p>The second component of the website will be the knowledge and learning area which will contain information and guidelines for the registered users to design and execute effective surveys, specimen survey questionnaires and how to use the different tools of the website.<a href="#">Learn more</a></p>


    <p><b>Modify your Questions</b></p>

    <p>The fourth component is preview where the user will be able to have an outlook of the survey in the same way the respondents will view in a web browser. This will allow for modifications in the design if necessary.<a href="#">Learn more</a></p>


    <p><b>Distribute and Execute</b></p>

    <p>The fifth component will be distribute and execute which will enable the user to send the survey to the target respondents. The survey may be distributed as web-links, QR Codes etc. to all or selected respondents.<a href="#">Learn more</a></p>


    <p><b>Analysis</b></p>

    <p>The seventh component of the website will be analyze which will enable the user to perform a number of analysis on the survey response data i.e. frequency distribution, measuring central tendency, measuring dispersion, hypothesis testing to name a few to draw meaningful pictures and outcomes.<a href="#">Learn more</a></p>


    <p><b>Report</b></p>

    <p>The eighth component of the survey will be reporting where the user will be able to create reports with the analysis or simply download the survey response data and analyses if different format i.e. print, pdf, doc, xlsx file etc.<a href="#">Learn more</a></p>


    <p><b>Evaluation</b></p>

    <p>The last component but not the least, evaluation will enable the users to evaluate and feedback the performance of the website during or after every usage. There may be options for live chat for assistance during survey phases too in the improved versions of the website. The feedback from the users may be used to further improve the design and functionality of the website.<a href="#">Learn more</a></p>


      
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

   <div class="clear"></div>
  </main>
</div>
    <!-- ################################################################################################ -->
 <div class="wrapper row4 bgded overlay" style="background-image:url('../images/demo/backgrounds/003.jpg');">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    
      
  
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Copyright &copy;All Rights Reserved - <a href="/">SurveyFun</a></p>
    <p class="fl_right">Privacy Policy | Terms & Conditions</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>
</body>
</html>