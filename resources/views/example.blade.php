<!DOCTYPE html>
<html lang="">
<head>
<title>SurveyFun</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">

<style type="text/css">
  .toggle-box {
  display: none;
}

.toggle-box + label {
  cursor: pointer;
  display: block;
  font-weight: bold;
  line-height: 21px;
  margin-bottom: 5px;
}

.toggle-box + label + div {
  display: none;
  margin-bottom: 10px;
}

.toggle-box:checked + label + div {
  display: block;
}

.toggle-box + label:before {
  background-color: #4F5150;
  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  border-radius: 10px;
  color: #FFFFFF;
  content: "+";
  display: block;
  float: left;
  font-weight: bold;
  height: 20px;
  line-height: 20px;
  margin-right: 5px;
  text-align: center;
  width: 20px;
}

.toggle-box:checked + label:before {
  content: "\2212";
}
</style>

</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="fl_left">
      <ul>
        <li><i class="fa fa-phone"></i> contact number</li>
        <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
      </ul>
    </div>
    <div class="fl_right">
      <ul>
        <li><a href="login"><i class="fa fa-lg fa-home"></i></a></li>
        <li><a href="login">Login</a></li>
        <li><a href="register">Register</a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
      <h1><a href="/">SurveyFun</a></h1>
      <p>Survey making website</p>
    </div>
    <div id="quickinfo" class="fl_right">
      <ul class="nospace inline">
        <li><strong>Help line:</strong><br>
          0000000000</li>
        <li><strong>Send mail:</strong><br>
          SurveyFun@gmail.com</li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </header>
  <nav id="mainav" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul class="clear">
      <li class="active"><a href="/">Home</a></li>

      <li><a href="about">About</a></li>
      <li><a href="feature">Features</a></li>
      <li><a href="register">Registration</a></li>
      <li><a href="example">Examples</a></li>
      <li><a href="service">Services</a></li>
      <li><a href="#">Help</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </nav>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/001.jpg');">
  <section id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
  
    <h6 class="heading">About SurveyFun</h6>
    <!-- ################################################################################################ -->
  </section>
</div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="sidebar third_quarter first"> 
      <!-- ################################################################################################ -->
      <h6>Questions Example</h6>
      <nav class="sdb_holder">
        <ul>
          <li><a href="#">Radio Button (Multiple Choice Question with One Answer)</a>
          <input class="toggle-box" id="header1" type="checkbox" >
          <label for="header1">Show more</label>
          <div>
          <img src="../images/example_image/1.png" alt="">
          </div>
          </li>
       
          <li><a href="#">Check Box (Check boxes are used for multiple select type questions)</a>
          <input class="toggle-box" id="header2" type="checkbox" >
          <label for="header2">Show more</label>
          <div>
          <img src="../images/example_image/2.png" alt="">
          </div>
          </li>
         

          <li><a href="#">Contact Information ( This question is used for gathering basic contact information, including First Name, Last Name, Email Address, Mailing Address, and Phone Number.)</a>
          <input class="toggle-box" id="header3" type="checkbox" >
          <label for="header3">Show more</label>
          <div>
          <img src="../images/example_image/3.png" alt="">
          </div>
          </li>


          <li><a href="#">Email Address ( This question is used for email addresses. When validated, only valid email addresses will be accepted.)</a>
          <input class="toggle-box" id="header4" type="checkbox" >
          <label for="header4">Show more</label>
          <div>
          <img src="../images/example_image/4.png" alt="">
          </div>
          </li>


          <li><a href="#">Comment Box (This is an Open Ended Text type question where users can input long text.)</a>
          <input class="toggle-box" id="header5" type="checkbox" >
          <label for="header5">Show more</label>
          <div>
          <img src="../images/example_image/5.png" alt="">
          </div>
          </li>


          <li><a href="#">Star Rating (Rank answer choices using stars.)</a>
          <input class="toggle-box" id="header6" type="checkbox" >
          <label for="header6">Show more</label>
          <div>
          <img src="../images/example_image/6.png" alt="">
          </div>
          </li>


          <li><a href="#">Text Slider (Users can slide the bar to show their desired preference)</a>
          <input class="toggle-box" id="header7" type="checkbox" >
          <label for="header7">Show more</label>
          <div>
          <img src="../images/example_image/7.png" alt="">
          </div>
          </li>


          <li><a href="#">Numeric Slider (Users can slide the bar to show their desired preference. Preferences will be numeric range.)</a>
          <input class="toggle-box" id="header8" type="checkbox" >
          <label for="header8">Show more</label>
          <div>
          <img src="../images/example_image/8.png" alt="">
          </div>
          </li>


          <li><a href="#">Thumbs Up/Down (Ask a binary rating question using a thumbs up or thumbs down image to help define the rating options.)</a>
          <input class="toggle-box" id="header9" type="checkbox" >
          <label for="header9">Show more</label>
          <div>
          <img src="../images/example_image/9.png" alt="">
          </div>
          </li>


          <li><a href="#">Smiley - Rating (The smiley question is a 5-point rating scale, intended to represent a range of sentiments from negative to neutral to positive.)</a>
          <input class="toggle-box" id="header10" type="checkbox" >
          <label for="header10">Show more</label>
          <div>
          <img src="../images/example_image/10.png" alt="">
          </div>
          </li>


          <li><a href="#">Rank Order (Rank order scaling questions allow a certain set of brands or products to be ranked based upon a specific attribute or characteristic.)</a>
          <input class="toggle-box" id="header11" type="checkbox" >
          <label for="header11">Show more</label>
          <div>
          <img src="../images/example_image/11.png" alt="">
          </div>
          </li>

        </ul>
      </nav>
    </div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

   <div class="clear"></div>
  </main>
</div>
    <!-- ################################################################################################ -->
 <div class="wrapper row4 bgded overlay" style="background-image:url('../images/demo/backgrounds/003.jpg');">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Copyright &copy;All Rights Reserved - <a href="/">SurveyFun</a></p>
    <p class="fl_right">Privacy Policy | Terms & Conditions</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>
</body>
</html>