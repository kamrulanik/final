

<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
    <title>SurveyFun</title>

    <title>Contact-Us Form With A Map Template | PrepBootstrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css" />

    <script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <link href="../css/layout.css" rel="stylesheet">



</head>
<body id="top">

<div class="wrapper row0">
    <div id="topbar" class="hoc clear">
        <!-- ################################################################################################ -->
        <div class="fl_left">
            <ul>
                <li><i class="fa fa-phone"></i> contact number</li>
                <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
            </ul>
        </div>
        <div class="fl_right">
            <ul>
                <li><a href="login"><i class="fa fa-lg fa-home"></i></a></li>
                <li><a href="login">Login</a></li>
                <li><a href="register">Register</a></li>
            </ul>
        </div>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
    <header id="header" class="hoc clear">
        <!-- ################################################################################################ -->
        <div id="logo" class="fl_left">
            <h1><a href="/">SurveyFun</a></h1>
            <p>Survey making website</p>
        </div>
        <div id="quickinfo" class="fl_right">
            <ul class="nospace inline">
                <li><strong>Help line:</strong><br>
                    0000000000</li>
                <li><strong>Send mail:</strong><br>
                    SurveyFun@gmail.com</li>
            </ul>
        </div>
        <!-- ################################################################################################ -->
    </header>
    <nav id="mainav" class="hoc clear">
        <!-- ################################################################################################ -->
        <ul class="clear">
            <li class="active"><a href="/">Home</a></li>

            <li><a href="about">About</a></li>
            <li><a href="feature">Features</a></li>
            <li><a href="register">Registration</a></li>
            <li><a href="example">Examples</a></li>
            <li><a href="service">Services</a></li>
            <li><a href="#">Help</a></li>
            <li><a href="create">Contact</a></li>
        </ul>
        <!-- ################################################################################################ -->
    </nav>
</div>





<div class="container">

    <div class="page-header">
        <h1>Contact-Us Form With A Map <small>   We are available for any types of query</small></h1>
    </div>

    <!-- Contact-Us Form With A Map - START -->


    <div class="container">
        <div><h1>Contact Us</h1></div>
        <br />
        <div class="row">
            <div class="col-md-6">
                <div id="googlemap" style="width:100%; height:350px;"></div>
            </div>
            <br />
            <div class="col-md-6">
                <form class="my-form" action="{{route('storeme')}}" method="post">
                    {{csrf_field()}}
                    <fieldset>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input name="name" type="text" class="form-control" id="form-name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <input name="email" type="text" class="form-control" id="form-email" placeholder="Email Address">
                        </div>
                        <div class="form-group">
                            <label for="subject">Telephone</label>
                            <input name="subject" type="text" class="form-control" id="form-subject" placeholder="Subject">
                        </div>
                        <div class="form-group">
                            <label for="message">Email your Message</label>
                            <textarea name="message" class="form-control" id="form-message" placeholder="Message"></textarea>
                        </div>
                        <button class="btn btn-default" type="submit">Contact Us</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>



    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            // Google Maps setup
            var uluru = {lat: 23.743051, lng: 90.373208};
            var googlemap = new google.maps.Map(
                document.getElementById('googlemap'),
                {
                    center: uluru,
                    zoom: 18,

                }
            );







        });


    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7nGs0-62KGWdUOvyr_w2dR3aNEjGVnY8&callback=initMap">
    </script>


    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>



    <!-- Contact-Us Form With A Map - END -->

</div>






<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/003.jpg');">
    <section class="hoc container clear">
        <!-- ################################################################################################ -->
        <div class="sectiontitle">
            <h6 class="heading">Execute Survey</h6>
            <p>Share your thought and questionnaires</p>
        </div>
        <ul class="nospace group center">
            <li class="one_quarter first">
                <article><a href="#"><i class="icon btmspace-30 fa fa-twitter"></i></a>
                    <h6 class="heading font-x1">Twitter</h6>
                    <p>Via twitter you can share your surveys and get survey responses which helps to get more data&hellip;</p>
                </article>
            </li>
            <li class="one_quarter">
                <article><a href="#"><i class="icon btmspace-30 fa fa-facebook"></i></a>
                    <h6 class="heading font-x1">Facebook</h6>
                    <p>Via Facebook you can share your surveys with your friends and get more responses which&hellip;</p>
                </article>
            </li>
            <li class="one_quarter">
                <article><a href="#"><i class="icon btmspace-30 fa fa-google-plus"></i></a>
                    <h6 class="heading font-x1">Google</h6>
                    <p>Via google plus you can share your surveys and get survey responses which helps to get more data&hellip;</p>
                </article>
            </li>
            <li class="one_quarter">
                <article><a href="#"><i class="icon btmspace-30 fa fa-linkedin"></i></a>
                    <h6 class="heading font-x1">Linkedin</h6>
                    <p>Via linkedin you can share your surveys and get survey responses which helps to get more data&hellip;</p>
                </article>
            </li>
        </ul>
        <!-- ################################################################################################ -->
    </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
    <div id="copyright" class="hoc clear">
        <!-- ################################################################################################ -->
        <p class="fl_left">Copyright &copy;All Rights Reserved - <a href="/">SurveyFun</a></p>
        <p class="fl_right">Privacy Policy | Terms & Conditions</p>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>
</body>
</html>