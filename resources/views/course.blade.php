@extends('layouts.app')

        <!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
    <title>SurveyFun</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://unpkg.com/jquery"></script>
    <script src="https://surveyjs.azureedge.net/1.0.32/survey.jquery.js"></script>

</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">




    <div id="topbar" class="hoc clear">

        <!-- ################################################################################################ -->
        <div class="fl_left">
            <ul>
                <li><i class="fa fa-phone"></i> contact number</li>
                <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
            </ul>
        </div>
        <div class="fl_right">

            <div class="container">

                <div class="collapse navbar-collapse" id="app-navbar-collapse">

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
        <!-- ################################################################################################ -->
    </div>
</div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
    <header id="header" class="hoc clear">
        <!-- ################################################################################################ -->
        <div id="logo" class="fl_left">
            <h1><a href="home">SurveyFun</a></h1>
            <p>Survey making website</p>
        </div>
        <div id="quickinfo" class="fl_right">
            <ul class="nospace inline">
                <li><strong>Help line:</strong><br>
                    0000000000</li>
                <li><strong>Send mail:</strong><br>
                    SurveyFun@gmail.com</li>
            </ul>
        </div>
        <!-- ################################################################################################ -->
    </header>
    <nav id="mainav" class="hoc clear">
        <!-- ################################################################################################ -->
        <ul class="clear">
            <li class="active"><a href="home">Home</a></li>
            <li><a>Design</a>

            </li>
            <li><a href="full-width">Your surveys</a>
                <!-- #<ul>
                  <li><a href="#">Level 2</a></li>
                  <li><a class="drop" href="#">Level 3</a>
                    <ul>
                      <li><a href="#">Level 3a</a></li>
                      <li><a href="#">Level 3b</a></li>
                      <li><a href="#">Level 3c</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Level 2</a></li>
                </ul>## -->
            </li>
            <li><a href="opinion">Opinion</a></li>
            <li><a href="#">Reports</a></li>
            <li><a href="survey/new.php">Evalution</a></li>
            <li><a href="ulabeva">Ulab Evalution</a></li>
        </ul>
        <!-- ################################################################################################ -->
    </nav>
</div>




<div  class="container">


    <style>
        * {box-sizing: border-box}
        body {font-family: Verdana, sans-serif; margin:0}
        .mySlides {display: none}
        img {vertical-align: middle;}

        /* Slideshow container */
        .slideshow-container {
            max-width: 1000px;
            position: relative;
            margin: auto;
        }

        /* Next & previous buttons */
        .prev, .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -22px;
            color: #900C3F;
            font-weight: bold;
            font-size: 18px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover, .next:hover {
            background-color: #E6B0AA;
        }

        /* Caption text */
        .text {
            color: #f2f2f2;
            font-size: 15px;
            padding: 8px 12px;
            position: absolute;
            bottom: 8px;
            width: 100%;
            text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
            cursor: pointer;
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;

        }

        .active, .dot:hover {
            background-color: #900C3F;
        }

        /* Fading animation */
        .fade {
            -webkit-animation-name: fade;
            -webkit-animation-duration: 300s;

            animation-duration: 300s;
        }

        @-webkit-keyframes fade {
            from {opacity: 1}
            to {opacity: 1}
        }

        @keyframes fade {
            from {opacity: 1}
            to {opacity: 1}
        }

        /* On smaller screens, decrease text size */
        @media only screen and (max-width: 300px) {
            .prev, .next,.text {font-size: 11px}
        }
    </style>





    @if($errors->any())
        @foreach($errors->all() as $error)

            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Oh snap!</strong>{{$error}}
                {{$error}}
            </div>

        @endforeach
    @endif




    <div  class="panel panel-default">
        <div style="background:#900C3F;" class="panel-heading">
            <h3 style="font-size: 15px;color: #ffffff" class="panel-title" align="center">Course Evaluation </h3>

        </div>

        <div  class="panel-body">


            <form class="form-horizontal" action="{{route('store')}}"    method="post">
                {{csrf_field()}}
                <fieldset>



                    <div class="slideshow-container" >

                        <div class="mySlides fade">


                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="a">1. Pease input survey ID</label>
                                <input style="width: 500px" name="a" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>

                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="b">2. Pease input Date</label>
                                <input style="width: 500px" name="b" type="date" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>
                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="c">3. Pease input your email</label>
                                <input style="width: 500px" name="c" type="email" class="form-control" id="formGroupExampleInput" placeholder="ulab@gmail.com"><span></span>


                            </div>
                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="d">4. Pease input course code</label>
                                <input style="width: 500px" name="d" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>

                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="e">5. Pease input Instructor code</label>
                                <input style="width: 500px" name="e" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>



                        </div>





                        <div class="mySlides fade">

                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="f">a) Has updated knowledge on the subject?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="f" type="text" class="form-control" id="formGroupExampleInput" placeholder="please input 1 or 2 or 3 or 4 or 5"><span></span>


                            </div>

                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="g">b) Makes topic easily understandable?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>
                                <input style="width: 500px" name="g" type="text" class="form-control" id="formGroupExampleInput" placeholder="please input 1 or 2 or 3 or 4 or 5">
                            </div>

                            <div style="float: bottom; margin-left: 100px" class="form-group">

                                <label for="h">c) Provides relevant meterials aparts from the text book?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>
                                <input style="width: 500px" name="h" type="text" class="form-control" id="formGroupExampleInput" placeholder="please input 1 or 2 or 3 or 4 or 5">
                            </div>

                        </div>

                        <div class="mySlides fade">

                            <div style="float: bottom; margin-left: 100px" class="form-group">

                                <label for="i">d) Has clear voice?</label>
                                <div><p>1. Strongly Agree</p> <p>2. Agree</p> <p>3. Neutral</p> <p>4. Disagree</p> <p>5. Strongly Disagree</p> </div>
                                <input style="width: 500px" name="i" type="text" class="form-control" id="formGroupExampleInput" placeholder="please input 1 or 2 or 3 or 4 or 5">
                            </div>
                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="j">e) Encourages students perticipation?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>
                                <input style="width: 500px" name="j" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                            </div>



                            <div style="float: bottom; margin-left: 100px" class="form-group">

                                <label for="k">f) Provides and maintains course outline?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>
                                <input style="width: 500px" name="k" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                            </div>

                        </div>


                        <div class="mySlides fade">

                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="l">g) Uses multiple techniques for students assesments?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="l" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>
                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="m">h) Maintance fairness and transparency?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="m" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>
                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="n">i) Provides timely feedback after every assesment?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="n" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>

                        </div>


                        <div class="mySlides fade">
                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="o">j) Motivate to students to perform?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="o" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>


                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="p">k) Provide guidance and conselling?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="p" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>
                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="q">l) Is a role model for the students?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="q" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>

                        </div>

                        <div class="mySlides fade">
                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="s">m) Showes positive attitude towards the students?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="s" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>
                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="r">n) Arrives and leaves the clean on time?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="r" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>


                            </div>
                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="s">o) Has updated knowledge on the subject?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="s" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>

                            </div>




                            <div style="float: bottom; margin-left: 100px" class="form-group">
                                <label for="t">p) Remains available during the specified consultation hours?</label>
                                <div><p>1.Strongly Agree</p> <p>2.Agree</p> <p>3.Neutral</p> <p>4.Disagree</p> <p>5.Strongly Disagree</p> </div>

                                <input style="width: 500px" name="t" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input"><span></span>

                            </div>


                        </div>


                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">

                                <button style="float: right; background: #900C3F" type="submit" class="btn btn-primary">Submit</button>
                            </div>

                        </div>

                        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                        <a class="next" onclick="plusSlides(1)">&#10095;</a>

                        <br>

                        <div style="text-align:center">
                            <span class="dot" onclick="currentSlide(1)"></span>
                            <span class="dot" onclick="currentSlide(2)"></span>
                            <span class="dot" onclick="currentSlide(3)"></span>
                            <span class="dot" onclick="currentSlide(4)"></span>
                            <span class="dot" onclick="currentSlide(5)"></span>
                            <span class="dot" onclick="currentSlide(6)"></span>
                        </div>



                    </div>






                </fieldset>
            </form>


            <div style="background:#900C3F; height: 10px;border-radius: 5px     ">



            </div>





        </div>

    </div>












</div>


</div>



</div>
<ul class="nospace group center">
    <li class="one_quarter first">
        <article><a href="#"><i class="icon btmspace-30 fa fa-twitter"></i></a>
            <h6 class="heading font-x1">Twitter</h6>
            <p>Via twitter you can share your surveys and get survey responses which helps to get more data&hellip;</p>
        </article>
    </li>
    <li class="one_quarter">
        <article><a href="#"><i class="icon btmspace-30 fa fa-facebook"></i></a>
            <h6 class="heading font-x1">Facebook</h6>
            <p>Via Facebook you can share your surveys with your friends and get more responses which&hellip;</p>
        </article>
    </li>
    <li class="one_quarter">
        <article><a href="#"><i class="icon btmspace-30 fa fa-google-plus"></i></a>
            <h6 class="heading font-x1">Google</h6>
            <p>Via google plus you can share your surveys and get survey responses which helps to get more data&hellip;</p>
        </article>
    </li>
    <li class="one_quarter">
        <article><a href="#"><i class="icon btmspace-30 fa fa-linkedin"></i></a>
            <h6 class="heading font-x1">Linkedin</h6>
            <p>Via linkedin you can share your surveys and get survey responses which helps to get more data&hellip;</p>
        </article>
    </li>
</ul>
<!-- ################################################################################################ -->
</section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
    <div id="copyright" class="hoc clear">
        <!-- ################################################################################################ -->
        <p class="fl_left">Copyright &copy;All Rights Reserved - <a href="/">SurveyFun</a></p>
        <p class="fl_right">Privacy Policy | Terms & Conditions</p>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script>


    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
    }
</script>



<script src="https://ajax.googleapis.com/ajax/libs/dojo/1.13.0/dojo/dojo.js"></script>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>



</body>
</html>