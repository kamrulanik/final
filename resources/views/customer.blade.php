

        <!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<html>
<head>
    <title>SurveyFun</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://unpkg.com/jquery"></script>
    <script src="https://surveyjs.azureedge.net/1.0.32/survey.jquery.js"></script>
    <link rel="stylesheet" href="../css/stylekm.css">
</head>
<body>
<div style="background: #900C3F"   class="panel-heading">
    <h3 style="font-size: 40px;color: #ffffff" class="panel-title" align="center">Customer Satisfaction Survey </h3>

</div>
<div id="surveyElement"></div>

<div id="result"></div>



<script type="text/javascript" src="./index.js"></script>

<script type="text/javascript">

    var json = {
        questions: [
            {
                type: "rating",
                name: "satisfaction",
                title: "How satisfied are you with the Product?",
                minRateDescription: "Not Satisfied",
                maxRateDescription: "Completely satisfied"




            },
            {
                type: "rating",
                name: "satisfaction",
                title: "How well does our product meet your needs?",
                minRateDescription: "Not Satisfied",
                maxRateDescription: "Completely satisfied"




            },

            {
                type: "rating",
                name: "satisfaction",
                title: "How easy is it to navigate our website?",
                minRateDescription: "Not Satisfied",
                maxRateDescription: "Completely satisfied"




            },

            {
                type: "rating",
                name: "satisfaction",
                title: "How much effort did you personally have to put forth to handle your request?",
                minRateDescription: "Not Satisfied",
                maxRateDescription: "Completely satisfied"




            },
            {
                type: "rating",
                name: "satisfaction",
                title: "How responsive have we been to your questions or concerns about our products?",
                minRateDescription: "Not Satisfied",
                maxRateDescription: "Completely satisfied"




            },
            {
                type: "rating",
                name: "satisfaction",
                title: "How satisfied are you with the Product?",
                minRateDescription: "Not Satisfied",
                maxRateDescription: "Completely satisfied"




            },
            {
                type: "rating",
                name: "satisfaction",
                title: "How satisfied are you with the Product?",
                minRateDescription: "Not Satisfied",
                maxRateDescription: "Completely satisfied"




            },
            {
                type: "rating",
                name: "satisfaction",
                title: "How satisfied are you with the Product?",
                minRateDescription: "Not Satisfied",
                maxRateDescription: "Completely satisfied"




            },
            {
                type: "rating",
                name: "satisfaction",
                title: "How satisfied are you with the Product?",
                minRateDescription: "Not Satisfied",
                maxRateDescription: "Completely satisfied"




            },
            {
                type: "rating",
                name: "satisfaction",
                title: "How satisfied are you with the Product?",
                minRateDescription: "Not Satisfied",
                maxRateDescription: "Completely satisfied"




            }




        ]




    };


    window.survey = new Survey.Model(json);

    survey
        .onComplete
        .add(function (result) {
            document
                .querySelector('#surveyResult')
                .innerHTML = "result: " + JSON.stringify(result.data);
        });

    $("#surveyElement").Survey({model: survey});



</script>

</body>
</html>

