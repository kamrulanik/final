<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
    <title>SurveyFun</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://unpkg.com/jquery"></script>
    <script src="https://surveyjs.azureedge.net/1.0.32/survey.jquery.js"></script>
    <link rel="stylesheet" href="../css/stylekm.css">
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">




    <div id="topbar" class="hoc clear">

        <!-- ################################################################################################ -->
        <div class="fl_left">
            <ul>
                <li><i class="fa fa-phone"></i> contact number</li>
                <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
            </ul>
        </div>
        <div class="fl_right">

            <div class="container">

                <div class="collapse navbar-collapse" id="app-navbar-collapse">

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
        <!-- ################################################################################################ -->
    </div>
</div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
    <header id="header" class="hoc clear">
        <!-- ################################################################################################ -->
        <div id="logo" class="fl_left">
            <h1><a href="home">SurveyFun</a></h1>
            <p>Survey making website</p>
        </div>
        <div id="quickinfo" class="fl_right">
            <ul class="nospace inline">
                <li><strong>Help line:</strong><br>
                    0000000000</li>
                <li><strong>Send mail:</strong><br>
                    SurveyFun@gmail.com</li>
            </ul>
        </div>
        <!-- ################################################################################################ -->
    </header>
    <nav id="mainav" class="hoc clear">
        <!-- ################################################################################################ -->
        <ul class="clear">
            <li class="active"><a href="home">Home</a></li>
            <li><a>Design</a>

            </li>
            <li><a href="full-width">Your surveys</a>
                <!-- #<ul>
                  <li><a href="#">Level 2</a></li>
                  <li><a class="drop" href="#">Level 3</a>
                    <ul>
                      <li><a href="#">Level 3a</a></li>
                      <li><a href="#">Level 3b</a></li>
                      <li><a href="#">Level 3c</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Level 2</a></li>
                </ul>## -->
            </li>
            <li><a href="opinion">Opinion</a></li>
            <li><a href="#">Reports</a></li>
            <li><a href="survey/new.php">Evalution</a></li>
            <li><a href="ulabeva">Ulab Evalution</a></li>
        </ul>
        <!-- ################################################################################################ -->
    </nav>
</div>


<div align="" style="width: 1000px; height: 300px; margin-left: 320px;margin-top: 60px" >
    <div align="center" style="background: #C39BD3"><h1 style="text-align: center;"></h1></div>
    <div id="surveyElement"></div>
    <div id="surveyResult"></div>

    <script type="text/javascript" src="./index.js"></script>
</div>


</div>
<div style="margin-top: 300px">
<ul style="margin-top: 100px" class="nospace group center">
    <li class="one_quarter first">
        <article><a href="#"><i class="icon btmspace-30 fa fa-twitter"></i></a>
            <h6 class="heading font-x1">Twitter</h6>
            <p>Via twitter you can share your surveys and get survey responses which helps to get more data&hellip;</p>
        </article>
    </li>
    <li class="one_quarter">
        <article><a href="#"><i class="icon btmspace-30 fa fa-facebook"></i></a>
            <h6 class="heading font-x1">Facebook</h6>
            <p>Via Facebook you can share your surveys with your friends and get more responses which&hellip;</p>
        </article>
    </li>
    <li class="one_quarter">
        <article><a href="#"><i class="icon btmspace-30 fa fa-google-plus"></i></a>
            <h6 class="heading font-x1">Google</h6>
            <p>Via google plus you can share your surveys and get survey responses which helps to get more data&hellip;</p>
        </article>
    </li>
    <li class="one_quarter">
        <article><a href="#"><i class="icon btmspace-30 fa fa-linkedin"></i></a>
            <h6 class="heading font-x1">Linkedin</h6>
            <p>Via linkedin you can share your surveys and get survey responses which helps to get more data&hellip;</p>
        </article>
    </li>
</ul>
</div>
<!-- ################################################################################################ -->
</section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
    <div id="copyright" class="hoc clear">
        <!-- ################################################################################################ -->
        <p class="fl_left">Copyright &copy;All Rights Reserved - <a href="/">SurveyFun</a></p>
        <p class="fl_right">Privacy Policy | Terms & Conditions</p>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->

<script type="text/javascript">

    Survey
        .StylesManager



    var json = {
        title: "Course Evaluation-Class",
        showProgressBar: "bottom",
        showTimerPanel: "top",
        maxTimeToFinishPage: 30,
        maxTimeToFinish: 300,
        firstPageIsStarted: true,
        startSurveyText: "Start Evaluation",
        pages: [



            {
                questions: [
                    {
                        type: "html",
                        html: "You are about to start quick survey of today's class. <br/>You have 30 seconds for every page and 300 seconds for the whole survey of 9 questions.<br/>Please click on <b>'Start Evaluation'</b> button when you are ready."
                    }
                ]
            },
            {
                title: "Please enter survey ID and your e-mail",
                questions: [

                    {
                        isRequired:true,
                        type: "text",
                        name: "S-ID",
                        title: "Survey ID:"
                    },
                    {
                        isRequired:true,
                        type: "text",
                        name: "Email",
                        title: "Your e-mail"
                    }
                ]
            },


            {
                questions: [
                    {
                        isRequired:true,
                        type: "radiogroup",
                        name: "Interest",
                        title: "Please rate your level of interest in this class.",
                        choices: [
                            "Very High", "1High", "Average", "Low", "Very Low","Don't Know"
                        ],

                    }
                ]
            }, {
                questions: [
                    {
                        isRequired:true,
                        type: "radiogroup",
                        name: "Quality",
                        title: "Please rate the quality of the learning material used in this class!.",
                        choices: [
                            "Very High", "1High", "Average", "Low", "Very Low","Don't Know"
                        ],

                    }
                ]
            },

            {

                questions: [

                    {
                        isRequired:true,
                        type: "text",
                        name: "Material",
                        title: "How can the material of this class be improved?"
                    },

                ]
            },




            {
                questions: [
                    {
                        isRequired:true,
                        type: "radiogroup",
                        name: "Class-Session",
                        title: "In terms of time, was the average class session-",
                        choices: [
                            "Far too long", "Too long", "Just right ", "Too short", "Far too short","Don't Know"
                        ],

                    }
                ]
            },

            {
                questions: [
                    {
                        isRequired:true,
                        type: "radiogroup",
                        name: "Assignment",
                        title: "Did the assignments support the overall objectives of this class?",
                        choices: [
                            "Always", "Most of the time", "Sometimes", "Rarely", "Never","Don't Know"
                        ],

                    }
                ]
            },

            {
                questions: [
                    {
                        isRequired:true,
                        type: "radiogroup",
                        name: "Reading",
                        title: "The amount of reading required for this class was-",
                        choices: [
                            "Far too much", "Too much", "Just right ", "Too little", "Far too little","Don't Know"
                        ],

                    }
                ]
            },


            {

                questions: [

                    {
                        isRequired:true,
                        type: "text",
                        name: "name",
                        title: "How can the structure of this class be improved?"
                    },

                ]
            },

        ],

    };




    window.survey = new Survey.Model(json);

    survey
        .onComplete
        .add(function (result) {
            document
                .querySelector('#surveyResult')
                .innerHTML = "result: " + JSON.stringify(result.data);
        });

    var survey = new Survey.Survey(json);
    survey.surveyPostId = "342ce9ae-fa4c-4aa6-bbcb-2acbe953f53f";
    //Optionally, show saving progress and show an error and "Save Again" button  if the results can't be stored
    survey.surveyShowDataSaving = true;

    $("#surveyElement").Survey({model: survey});


</script>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>
</body>
</html>