@extends('layouts.app2')
@section('content')


    <div class="container">

        @if(session('successMsg'))
            <div class="alert alert-dismissible alert-info">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Well Done!</strong> {{session('successMsg')}}
            </div>
        @endif
    </div>


    <div style="border-radius: 10px" class="panel panel-default">
        <div style="background: #900C3F"   class="panel-heading">
            <h3 style="font-size: 40px;color: #ffffff" class="panel-title" align="center">Course Evaluation Report </h3>

        </div>


        <table class="table table-bordered table-striped table-hover ">
            <thead>
            <tr style="background: #E6B0AA"  >
                <th class="text-center">ID</th>
                <th class="text-center" >S-ID</th>
                <th class="text-center">Date</th>
                <th class="text-center">Email</th>
                <th class="text-center">C-Code</th>
                <th class="text-center">I-Code</th>
                <th class="text-center">Q-a</th>
                <th class="text-center">Q-b</th>
                <th class="text-center" >Q-c</th>
                <th class="text-center">Q-d</th>
                <th class="text-center">Q-e</th>
                <th class="text-center">Q-f</th>
                <th class="text-center">Q-g</th>
                <th class="text-center">Q-h</th>
                <th class="text-center">Q-i</th>
                <th class="text-center">Q-j</th>
                <th class="text-center">Q-k</th>
                <th class="text-center">Q-l</th>
                <th class="text-center" >Q-m</th>
                <th class="text-center">Q-n</th>
                <th class="text-center">Q-o</th>


                <th class="text-center">Action</th>

            </tr>
            </thead>
            <tbody>
            @foreach($students as $student)
                <tr class="text-center">
                    <td class="text-center">{{$student->id}}</td>
                    <td class="text-center">{{$student->a}}</td>
                    <td class="text-center">{{$student->b}}</td>
                    <td class="text-center">{{$student->c}}</td>
                    <td class="text-center">{{$student->d}}</td>
                    <td class="text-center">{{$student->e}}</td>
                    <td class="text-center">{{$student->f}}</td>

                    <td class="text-center">{{$student->g}}</td>
                    <td class="text-center">{{$student->h}}</td>
                    <td class="text-center">{{$student->i}}</td>
                    <td class="text-center">{{$student->j}}</td>
                    <td class="text-center">{{$student->k}}</td>
                    <td class="text-center">{{$student->l}}</td>
                    <td class="text-center">{{$student->m}}</td>

                    <td class="text-center">{{$student->n}}</td>
                    <td class="text-center">{{$student->o}}</td>
                    <td class="text-center">{{$student->p}}</td>
                    <td class="text-center">{{$student->q}}</td>
                    <td class="text-center">{{$student->r}}</td>
                    <td class="text-center">{{$student->s}}</td>
                    <td class="text-center">{{$student->t}}</td>
                    <td class="text-center">

                        <form method="POST" id="delete-form-{{$student->id}}" action="{{route('delete',$student->id)}}" style="display: none;">
                            {{csrf_field()}}
                            {{method_field('delete')}}

                        </form>



                        <button style="background: #900C3F"  onclick="if (confirm('Are You Want to Delete This')){
                                event.preventDefault();
                                document.getElementById('delete-form-{{$student->id}}').submit();
                                }else {
                                event.preventDefault();
                                }" class="btn btn-raised btn-danger btn-sm" href="">Delete
                        </button> </td>

                </tr>
            @endforeach
            </tbody>
        </table>




@endsection