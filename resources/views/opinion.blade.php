@extends('layouts.app')

@section('content')


    <div class="container">
        @include('flash::message')
        <div class="row justify-content-center">
            <div class="col-md-12">
                {!! Form::open() !!}
                <div class="card">
                    <div style="float: right; color: #ffffff" class="card-header">Add a new Review or Survey</div>

                    <div  style=" background: #900C3F  ; border-radius: 5px  " class="card-body">
                        <div class="form-group">
                            <label style="color: #ffffff">Write a new Review</label>

                            <textarea required class="form-control" name="status-text" id="status-text">  </textarea>


                        </div>
                    </div>
                    <div class="panel-footer clearfix">


                        <button style="float: right; background: #900C3F" class="btn btn-info float-right btn-sm"><i class="fa fa-plus" style="font-size:16px;"></i> Add Review </button>
                        <p>All Reviews</p>

                    </div>
                    {!! Form::close() !!}

                    @foreach($top_15_posts as $status)
                        {!!
                         view('layouts.app-internal.user-status-layout',[
                        'status'=>$status,
                        'user' => \App\Eloquent\User::find($status->users_id),
                        'comments'=>\App\Eloquent\StatusComments::where('status_id', $status->id)->orderBy('id','DESC')->get(),
                        'comment_count'=>\App\Eloquent\StatusComments::where('status_id', $status->id)->count(),
                        'like_count'  =>\App\Eloquent\StatusLikes::where('status_id', $status->id)->count(),
                        ])
                        !!}
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection
