

        <!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
<title>SurveyFun</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="fl_left">
      <ul>
        <li><i class="fa fa-phone"></i> contact number</li>
        <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
      </ul>
    </div>
    <div class="fl_right">
      <ul>
        <li><a href="login"><i class="fa fa-lg fa-home"></i></a></li>
        <li><a href="login">Login</a></li>
        <li><a href="register">Register</a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
      <h1><a href="/">SurveyFun</a></h1>
      <p>Survey making website</p>
    </div>
    <div id="quickinfo" class="fl_right">
      <ul class="nospace inline">
        <li><strong>Help line:</strong><br>
          0000000000</li>
        <li><strong>Send mail:</strong><br>
          SurveyFun@gmail.com</li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </header>
  <nav id="mainav" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul class="clear">
      <li class="active"><a href="/">Home</a></li>

      <li><a href="about">About</a></li>
      <li><a href="feature">Features</a></li>
      <li><a href="register">Registration</a></li>
      <li><a href="example">Examples</a></li>
      <li><a href="service">Services</a></li>
      <li><a href="#">Help</a></li>
      <li><a href="create">Contact</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </nav>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/01.jpg');">
  <div id="pageintro" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <article>
      <p><b>Survey creativity</b></p>
      <h3 class="heading">Online surveys in minutes</h3>
      <p><b>Design your survey, Make smart decision, Better execution, Exact result, Saving time and cost, Flexible, Easy to use for any organizations and also individuals.</b></p>
      <footer>
        <ul class="nospace inline pushright">
          <li><a class="btn" href="register">Signup Free</a></li>
          <li><a class="btn inverse" href="login">Create a Survey</a></li>
        </ul>
      </footer>
    </article>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="sectiontitle">
      <h6 class="heading">SurveyFun</h6>
      <p>Customize your design, set questionnaires and execute.</p>
    </div>
    <div class="group">
      <div class="one_half first">
        <p>Why <b>SurveyFun ?</b></p>
        <p class="btmspace-50">The Website aims to design and develop an online survey system which will provide its users the required knowledge and tools required to design surveys, preview, distribute and execute surveys on the respondents, accumulate and organize survey response data, analyze survey data and generate reports in convenient formats. The website will allow the users to execute their surveys through web browsers of any device having internet connectivity and web-brewers like PC, Cell Phones etc.</p>
        <ul class="nospace group">
          <li class="one_half first">
            <article><a href="#"><i class="icon btmspace-30 fa fa-industry"></i></a>
              <h6 class="heading font-x1">For Industry</h6>
              <p>Businesses and researchers across all industries conduct surveys to uncover answers to specific,important questions.&hellip;</p>
            </article>
          </li>
          <li class="one_half">
            <article><a href="#"><i class="icon btmspace-30 fa fa-institution"></i></a>
              <h6 class="heading font-x1">For Institution</h6>
              <p>Survey can be exceptionally useful for associations in Bangladesh. However, the act of this professional activity by&hellip;</p>
            </article>
          </li>
        </ul>
      </div>
      <div class="one_half"><img class="inspace-10 borderedbox" src="images/demo/backgrounds/index2.jpg" alt=""></div>
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/003.jpg');">
  <section class="hoc container clear"> 
    <!-- ################################################################################################ -->
    <div class="sectiontitle">
      <h6 class="heading">Execute Survey</h6>
      <p>Share your thought and questionnaires</p>
    </div>
    <ul class="nospace group center">
      <li class="one_quarter first">
        <article><a href="#"><i class="icon btmspace-30 fa fa-twitter"></i></a>
          <h6 class="heading font-x1">Twitter</h6>
          <p>Via twitter you can share your surveys and get survey responses which helps to get more data&hellip;</p>
        </article>
      </li>
      <li class="one_quarter">
        <article><a href="#"><i class="icon btmspace-30 fa fa-facebook"></i></a>
          <h6 class="heading font-x1">Facebook</h6>
          <p>Via Facebook you can share your surveys with your friends and get more responses which&hellip;</p>
        </article>
      </li>
      <li class="one_quarter">
        <article><a href="#"><i class="icon btmspace-30 fa fa-google-plus"></i></a>
          <h6 class="heading font-x1">Google</h6>
          <p>Via google plus you can share your surveys and get survey responses which helps to get more data&hellip;</p>
        </article>
      </li>
      <li class="one_quarter">
        <article><a href="#"><i class="icon btmspace-30 fa fa-linkedin"></i></a>
          <h6 class="heading font-x1">Linkedin</h6>
          <p>Via linkedin you can share your surveys and get survey responses which helps to get more data&hellip;</p>
        </article>
      </li>
    </ul>
    <!-- ################################################################################################ -->
  </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Copyright &copy;All Rights Reserved - <a href="/">SurveyFun</a></p>
    <p class="fl_right">Privacy Policy | Terms & Conditions</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>
</body>
</html>