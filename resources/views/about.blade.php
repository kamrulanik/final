<!DOCTYPE html>
<html lang="">
<head>
<title>SurveyFun</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="fl_left">
      <ul>
        <li><i class="fa fa-phone"></i> contact number</li>
        <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
      </ul>
    </div>
    <div class="fl_right">
      <ul>
        <li><a href="login"><i class="fa fa-lg fa-home"></i></a></li>
        <li><a href="login">Login</a></li>
        <li><a href="register">Register</a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
      <h1><a href="/">SurveyFun</a></h1>
      <p>Survey making website</p>
    </div>
    <div id="quickinfo" class="fl_right">
      <ul class="nospace inline">
        <li><strong>Help line:</strong><br>
          0000000000</li>
        <li><strong>Send mail:</strong><br>
          SurveyFun@gmail.com</li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </header>
  <nav id="mainav" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul class="clear">
      <li class="active"><a href="/">Home</a></li>

      <li><a href="about">About</a></li>
      <li><a href="feature">Features</a></li>
      <li><a href="register">Registration</a></li>
      <li><a href="example">Examples</a></li>
      <li><a href="service">Services</a></li>
      <li><a href="#">Help</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </nav>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/001.jpg');">
  <section id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
  
    <h6 class="heading">About SurveyFun</h6>
    <!-- ################################################################################################ -->
  </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="sidebar one_quarter first"> 
      <!-- ################################################################################################ -->
      <h6>About</h6>
      <nav class="sdb_holder">
        <ul>
          <li><a href="#">Solutions</a> </li>
          
            <ul>
              <li><a href="#">Education</a>
              <li><a href="#">Government</a></li>
              <li><a href="#">Business</a></li>
              <li><a href="#">Medical</a></li>
              <li><a href="#">Non Profits</a></li>
            </ul>
         

          <li><a href="#">Features</a> </li>
          
            <ul>
              <li><a href="#">Custom Designs</a></li>
              <li><a href="#">Easy to Use</a></li>
              <li><a href="#">Question Types</a></li>
              <li><a href="#">Execute</a></li>
              </ul>
         
                  
          <li><a href="#">Terms of Use</a></li>

          <li><a href="#">Privacy Policy</a></li>

          <li><a href="#">Contact</a></li>
        </ul>
      </nav>
       </div>
      
      <!-- ################################################################################################ -->
   
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="content three_quarter"> 
      <!-- ################################################################################################ -->
      <h1>Who We Are and What We provide</h1>
      <img class="imgr borderedbox inspace-5" src="../images/demo/backgrounds/aboutS.png" alt="">
      <p>The “SurveyFun” is a web based survey system which helps people to create their account, design or make a survey, execute and also get a report.</p>
      <p>In this system user gets all the details after opening an account. The new user first registers them by applying at the SurveyFun site. After verifying the details about new users request and activate the users account. User will get verification e-mail after any type of update in the account.</p>
      <img class="imgl borderedbox inspace-5" src="../images/demo/backgrounds/aboutS1.jpg" alt="">
      <p>Online Survey System project captures performed by different roles in real life survey which provides enhanced techniques for maintaining the required information up-to-date which results in efficiency. The project gives real life understanding of Online Survey System and Activities performed by various roles in the supply chain. New users can register through online application form which is available in our website. After registration the system, it will help users to design the survey by drag and drop, and also give them the knowledge of question type which helps them to make a good survey paper. For the help of online survey system users can easily get the result and report.</p>

      <p><br> The application is to be fully-functional survey software. It will consist of a few different modules:-
The first module is to be a software application that can be used by website admin. This application will allow the user to open new account, check surveys and histories.
The second module is to be a software application that is used within the website will send verification e-mail to the new user. This application will allow everything the Online allows as well as some additional features. These features include; account creation, survey creation, user’s records, and reports.
This website will handle multiple threads and will therefore allow for simultaneous access of multiple users. It will provide for user authentication and will store all data.</p>
     
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

   <div class="clear"></div>
  </main>
</div>
    <!-- ################################################################################################ -->
 <div class="wrapper row4 bgded overlay" style="background-image:url('../images/demo/backgrounds/003.jpg');">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    
      
  
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Copyright &copy;All Rights Reserved - <a href="/">SurveyFun</a></p>
    <p class="fl_right">Privacy Policy | Terms & Conditions</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>
</body>
</html>