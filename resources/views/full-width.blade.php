<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
<title>SurveyFun | Design | Demo interface</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="stylesheet" href="../css/jquery-ui.css">
<link rel="stylesheet" href="../css/custom.css">

<link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Special version of Bootstrap that only affects content wrapped in .bootstrap-iso -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

<!-- Inline CSS based on choices in "Settings" tab -->

<style>

  
@import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);


</style>

<script>
  
$(document).ready(function(){

  $("#myAccordion").accordion();
  $( ".draggable li" ).draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid"
    });
  //$(".draggable li").draggable({helper:"clone"});
  $( "#sortable" ).sortable({
      revert: true
    });
  //$("#sortable").droppable({drop:function(event,ui){
   // $("#items").append($("<li></li>").text(ui.draggable.text()));
  //}});
  //$( "ul, li" ).disableSelection();
});

$("#add").click(function() {
  var table = $("#customers");
  table.append(table.find("tr:eq(1)").clone());

});


</script>

 </head>
<body>
<!-- ################################################################## -->
  <h2>Content</h2>
<div id="myAccordion">
<!-- ################################################################## -->

  <h3>Login Form</h3>
  <ul class="draggable">
    <li>
    
        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" class="form-control" name="username" placeholder="Username">
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" name="password" placeholder="password">
        </div>
      </form>
      </li>
  </ul>
<!-- ################################################################## -->
  <h3>Check box selection</h3>
  <ul class="draggable">
    <li>
<form>
  <textarea rows="1" cols="50" name="comment" form="usrform">What types of credit cards do you have(Select all that apply)?</textarea>
    <div class="checkbox">
      <label><input type="checkbox" value=""><input type="text" name="usrname" value="Visa">
  
    </div>
    <div class="checkbox">
      <label><input type="checkbox" value=""><input type="text" name="usrname" value="Mastercard">
  
    </div>
    <div class="checkbox">
      <label><input type="checkbox" value=""><input type="text" name="usrname" value="American Express">
  
    </div>
    
  <!-- ########<input type="submit">################# -->
  </form>

</li>
  </ul>
<!-- ################################################################## -->
  <h3>Contact Form</h3>
  <ul class="draggable">
    <li> <form action="/action_page.php" class="w3-container w3-card-4 w3-light-grey w3-text-blue w3-margin">
<h2 class="w3-center">Contact Us</h2>
 
<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="first" type="text" placeholder="First Name">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="last" type="text" placeholder="Last Name">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-envelope-o"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="email" type="text" placeholder="Email">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-phone"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="phone" type="text" placeholder="Phone">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-pencil"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="message" type="text" placeholder="Message">
    </div>
</div>

<button class="w3-button w3-block w3-section w3-blue w3-ripple w3-padding">Send</button>

</form>

  </li>
  </ul>
  <!-- ################################################################## -->
<h3>Multiple choise question</h3>
  <ul class="draggable">
    <li>
<form action="">
  <textarea rows="1" cols="50" name="comment" form="usrform">How often do you conduct surveys?</textarea><br>
  <input type="radio" name="gender" value="Male"><input type="text" name="usrname" value="Weekly"><br>
  <input type="radio" name="gender" value="Female"><input type="text" name="usrname" value="Monthly"><br>
  <input type="radio" name="gender" value="Other"><input type="text" name="usrname" value="Annually"><br>

  <!-- ########<input type="submit">################# -->
  </form>

</li>
  </ul>
  <!-- ################################################################## -->
<h3>Star Rating</h3>
  <ul class="draggable">
    <li>
  <textarea rows="1" cols="50" name="comment" form="usrform">What do you about this website?</textarea><br>

<fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
</fieldset>

</li>
  </ul>

  <!-- ################################################################## -->
<h3>Star Rating</h3>
  <ul class="draggable">
    <li>
<form action="../website.dev/create">
    <input type="submit" value="Go to website" />
</form>
</li>
  </ul>

  <!-- ################################################################## -->



  <!-- ################################################################## -->

</div>

<div class="panel-heading">Drop your form</div>
<div id="sortable">
  <br>
  <ol id="items"></ol>
  
  
</div>







</body>

</html>
