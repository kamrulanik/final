<!DOCTYPE html>
<html lang="">
<head>
<title>SurveyFun</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="fl_left">
      <ul>
        <li><i class="fa fa-phone"></i> contact number</li>
        <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
      </ul>
    </div>
    <div class="fl_right">
      <ul>
        <li><a href="login"><i class="fa fa-lg fa-home"></i></a></li>
        <li><a href="login">Login</a></li>
        <li><a href="register">Register</a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
      <h1><a href="/">SurveyFun</a></h1>
      <p>Survey making website</p>
    </div>
    <div id="quickinfo" class="fl_right">
      <ul class="nospace inline">
        <li><strong>Help line:</strong><br>
          0000000000</li>
        <li><strong>Send mail:</strong><br>
          SurveyFun@gmail.com</li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </header>
  <nav id="mainav" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul class="clear">
      <li class="active"><a href="/">Home</a></li>

      <li><a href="about">About</a></li>
      <li><a href="feature">Features</a></li>
      <li><a href="register">Registration</a></li>
      <li><a href="example">Examples</a></li>
      <li><a href="service">Services</a></li>
      <li><a href="#">Help</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </nav>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/001.jpg');">
  <section id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
  
    <h6 class="heading">Features of SurveyFun</h6>
    <!-- ################################################################################################ -->
  </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="sidebar one_quarter first"> 
      <!-- ################################################################################################ -->
      <h6>About</h6>
      <nav class="sdb_holder">
        <ul>
          <li><a href="#">Solutions</a> </li>
          
            <ul>
              <li><a href="#">Education</a>
              <li><a href="#">Government</a></li>
              <li><a href="#">Business</a></li>
              <li><a href="#">Medical</a></li>
              <li><a href="#">Non Profits</a></li>
            </ul>
         

          <li><a href="#">Features</a> </li>
          
            <ul>
              <li><a href="#">Custom Designs</a></li>
              <li><a href="#">Easy to Use</a></li>
              <li><a href="#">Question Types</a></li>
              <li><a href="#">Execute</a></li>
              </ul>
         
                  
          <li><a href="#">Terms of Use</a></li>

          <li><a href="#">Privacy Policy</a></li>

          <li><a href="#">Contact</a></li>
        </ul>
      </nav>
       </div>
      
      <!-- ################################################################################################ -->
   
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="content three_quarter"> 
      <!-- ################################################################################################ -->
  
 
    <!-- ################################################################################################ -->
    <div class="sectiontitle">
      <h6 class="heading">Survey Features</h6>
      <p>First of all I want to make a website which can provide knowledge, skill, design a survey, execute, accumulate, analyze, reporting and evolution this web site and many more to go.</p>
    </div>
    <ul class="nospace group services">
      <li class="one_quarter first">
        <article class="inverse">
          <h6 class="heading font-x1"><a href="../images/demo/backgrounds/dnd.png">Drag n Drop</a></h6>
          <img class="imgl borderedbox inspace-5" src="../images/demo/backgrounds/dnd.png" alt="">
          <p>Easy to use;All contents are given in a sidebar;they can be drag and drop as needed and the user..</p>
        </article>
      </li>

      <li class="one_quarter">
        <article>
          <h6 class="heading font-x1"><a href="../images/demo/backgrounds/q.jpg">Question Types</a></h6>
          <img class="imgl borderedbox inspace-5" src="../images/demo/backgrounds/q.jpg" alt="">
          <p>Question are define with example for that people can..</p>
        </article>
      </li>

      <li class="one_quarter">
        <article class="inverse">
          <h6 class="heading font-x1"><a href="../images/demo/backgrounds/knowledge.jpg">Knowledge</a></h6>
          <img class="imgl borderedbox inspace-5" src="../images/demo/backgrounds/knowledge.jpg" alt="">
          <p>Providing guide or leaning item which helps new users to know about the website,learn how to use..</p>
        </article>
      </li>

      <li class="one_quarter">
        <article>
          <h6 class="heading font-x1"><a href="../images/demo/backgrounds/advanced.jpg">Advanced Page Jumping</a></h6>
          <img class="imgl borderedbox inspace-5" src="../images/demo/backgrounds/advanced.jpg" alt="">
          <p>Advanced page jumping feature customized path through your survey..</p>
        </article>
      </li>


    </ul>
<!-- ################################################################################################ -->
<br>

<ul class="nospace group services">
      <li class="one_quarter first">
        <article class="inverse">
          <h6 class="heading font-x1"><a href="../images/demo/backgrounds/execute.png">Execute</a></h6>
          <img class="imgl borderedbox inspace-5" src="../images/demo/backgrounds/execute.png" alt="">
          <p>To execute survey they can explore their paper through email.</p>
        </article>
      </li>

      <li class="one_quarter">
        <article>
          <h6 class="heading font-x1"><a href="../images/demo/backgrounds/piping.gif">Question Answer Piping</a></h6>
          <img class="imgl borderedbox inspace-5" src="../images/demo/backgrounds/piping.gif" alt="">
          <p>Using piping to insert previous responses into question text or answer choices.</p>
        </article>
      </li>

      <li class="one_quarter">
         <article class="inverse">
          <h6 class="heading font-x1"><a href="../images/demo/backgrounds/reporting.png">Reporting</a></h6>
          <img class="imgl borderedbox inspace-5" src="../images/demo/backgrounds/reporting.png" alt="">
          <p>Analyze results,users can download the report as a pdf, doc, xlsx file etc.</p>
        </article>
      </li>

      <li class="one_quarter">
        <article>
          <h6 class="heading font-x1"><a href="../images/demo/backgrounds/evolution.png">Evolution</a></h6>
          <img class="imgl borderedbox inspace-5" src="../images/demo/backgrounds/evolution.png" alt="">
          <p>Evaluate the website;How much beneficial and give a suggestion for improvement</p>
        </article>
      </li>

      
    </ul>

 
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="clear"></div>
</main>
</div>
    <!-- ################################################################################################ -->
 <div class="wrapper row4 bgded overlay" style="background-image:url('../images/demo/backgrounds/003.jpg');">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    
      
  
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Copyright &copy;All Rights Reserved - <a href="/">SurveyFun</a></p>
    <p class="fl_right">Privacy Policy | Terms & Conditions</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>
</body>
</html>