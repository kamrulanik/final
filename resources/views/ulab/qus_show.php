<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript">
	function timeout()
	{
		//var hours=Math.floor(timeLeft/300);
		var minute=Math.floor(timeLeft/300);
		var second=timeLeft%60;
		//var hrs=checktime(hours);
		var mint=checktime(minute);
		var sec=checktime(second);
		if(timeLeft<=0)
		{
			clearTimeout(tm);
			document.getElementById("form1").submit();
		}
		else
		{

			document.getElementById("time").innerHTML=mint+":"+sec;
		}
		timeLeft--;
		var tm= setTimeout(function(){timeout()},1000);
	}
	function checktime(msg)
	{
		if(msg<10)
		{
			msg="0"+msg;
		}
		return msg;
	}
	</script>
  
</head>
<body id="top">



<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="fl_left">
      <ul>
        <li><i class="fa fa-phone"></i> contact number</li>
        <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
      </ul>
    </div>
    <div class="fl_right">
      
        <div class="container">

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                   
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
      <h1><a href="/">SurveyFun</a></h1>
      <p>Survey making website</p>
    </div>
    <div id="quickinfo" class="fl_right">
      <ul class="nospace inline">
        <li><strong>Help line:</strong><br>
          0000000000</li>
        <li><strong>Send mail:</strong><br>
          SurveyFun@gmail.com</li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </header>
  <nav id="mainav" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul class="clear">
      <li class="active"><a href="home">Home</a></li>
      <li><a class="drop">Design</a>
        <ul>
          <li><a href="design">Demo interface</a></li>
          <li><a href="form">Form</a></li>
          <li><a href="survey/view">Level 2</a></li>
          <li><a href="#">Level 3</a></li>
        </ul>
      </li>
      <li><a href="full-width">Your surveys</a>
        <!-- #<ul>
          <li><a href="#">Level 2</a></li>
          <li><a class="drop" href="#">Level 3</a>
            <ul>
              <li><a href="#">Level 3a</a></li>
              <li><a href="#">Level 3b</a></li>
              <li><a href="#">Level 3c</a></li>
            </ul>
          </li>
          <li><a href="#">Level 2</a></li>
        </ul>## -->
      </li>
      <li><a href="#">Analyze</a></li>
      <li><a href="#">Reports</a></li>
      <li><a href="quiz_game">Evalution</a></li>
      <li><a href="ulab/home.php">Ulab Evalution</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </nav>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('../images/demo/backgrounds/a3.jpg');">
<section id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul>
      <li><a href="home">Home</a></li>
      <li><a href="about">About</a></li>
      <li><a href="feature">Features</a></li>
      <li><a href="register">Registration</a></li>
      <li><a href="example">Examples</a></li>
      <li><a href="service">Services</a></li>
      <li><a href="#">Help</a></li>
    </ul>
    <!-- ################################################################################################ -->
    <h6 class="heading">Survey Design</h6>
    <!-- ################################################################################################ -->
  </section>
</div>
<body onload="timeout()" >

<div class="container">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
		  <h2>Onilne Evalution system
		  <script type="text/javascript">
		  var timeLeft=3000;
		  </script>
		  
		  <div id="time"style="float:right">timeout</div></h2>
		
		<form method="post" id="form1" action="answer.php">
		 <table class="table table-bordered">
		 	<ul>
		 	<li>

			 1) Has updated knowledge on the subject?</li>
			  <input type="radio" name="Agree" value="Agree" checked> Agree<br>
			  <input type="radio" name="Agree" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="Agree" value="Nutral">Nutral <br> 
			  <input type="radio" name="Agree" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="Agree" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
			<li>
			2) Makes topic easily understandable?</li>
			  <input type="radio" name="Agre" value="Agree" checked> Agree<br>
			  <input type="radio" name="Agre" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="Agre" value="Nutral">Nutral <br> 
			  <input type="radio" name="Agre" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="Agre" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 3) Provides relevant meterials aparts from the text book?</li>
			  <input type="radio" name="Agr" value="Agree" checked> Agree<br>
			  <input type="radio" name="Agr" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="Agr" value="Nutral">Nutral <br> 
			  <input type="radio" name="Agr" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="Agr" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 4) Has clear voice?</li>
			  <input type="radio" name="Ag" value="Agree" checked> Agree<br>
			  <input type="radio" name="Ag" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="Ag" value="Nutral">Nutral <br> 
			  <input type="radio" name="Ag" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="Ag" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 5) Encourages students perticipation?</li>
			  <input type="radio" name="A" value="Agree" checked> Agree<br>
			  <input type="radio" name="A" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="A" value="Nutral">Nutral <br> 
			  <input type="radio" name="A" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="A" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 6) Provides and maintains course outline?</li>
			  <input type="radio" name="DisAgree" value="Agree" checked> Agree<br>
			  <input type="radio" name="DisAgree" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="DisAgree" value="Nutral">Nutral <br> 
			  <input type="radio" name="DisAgree" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="DisAgree" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 7) Uses multiple techniques for students assesments?</li>
			  <input type="radio" name="DisAgre" value="Agree" checked> Agree<br>
			  <input type="radio" name="DisAgre" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="DisAgre" value="Nutral">Nutral <br> 
			  <input type="radio" name="DisAgre" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="DisAgre" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 8) Maintance fairness and transparency?</li>
			  <input type="radio" name="DisAgr" value="Agree" checked> Agree<br>
			  <input type="radio" name="DisAgr" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="DisAgr" value="Nutral">Nutral <br> 
			  <input type="radio" name="DisAgr" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="DisAgr" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
		<ul>
		 	<li>

			 9) Provides timely feedback after every assesment?</li>
			  <input type="radio" name="DisAg" value="Agree" checked> Agree<br>
			  <input type="radio" name="DisAg" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="DisAg" value="Nutral">Nutral <br> 
			  <input type="radio" name="DisAg" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="DisAg" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 10) Motivate to students to perform?</li>
			  <input type="radio" name="DisA" value="Agree" checked> Agree<br>
			  <input type="radio" name="DisA" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="DisA" value="Nutral">Nutral <br> 
			  <input type="radio" name="DisA" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="DisA" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 11) Provide guidance and conselling?</li>
			  <input type="radio" name="Dis" value="Agree" checked> Agree<br>
			  <input type="radio" name="Dis" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="Dis" value="Nutral">Nutral <br> 
			  <input type="radio" name="Dis" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="Dis" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 12) Is a role model for the students?</li>
			  <input type="radio" name="Di" value="Agree" checked> Agree<br>
			  <input type="radio" name="Di" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="Di" value="Nutral">Nutral <br> 
			  <input type="radio" name="Di" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="Di" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 13) Showes positive attitude towards the students?</li>
			  <input type="radio" name="D" value="Agree" checked> Agree<br>
			  <input type="radio" name="D" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="D" value="Nutral">Nutral <br> 
			  <input type="radio" name="D" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="D" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 14) Arrives and leaves the clean on time?</li>
			  <input type="radio" name="Arrives" value="Agree" checked> Agree<br>
			  <input type="radio" name="Arrives" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="Arrives" value="Nutral">Nutral <br> 
			  <input type="radio" name="Arrives" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="Arrives" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
			<ul>
		 	<li>

			 15) Remains available during the specified consultation hours?</li>
			  <input type="radio" name="Remains" value="Agree" checked> Agree<br>
			  <input type="radio" name="Remains" value="Highly Agree"> Highly Agree<br>
			  <input type="radio" name="Remains" value="Nutral">Nutral <br> 
			  <input type="radio" name="Remains" value="DisAgree" checked>Dis Agree<br>
			  <input type="radio" name="Remains" value="Highly DisAgree"> Highly DisAgree<br>
			</ul>
	</table>
  <!-- ########<input type="submit">################# -->
  	  
<center><input type="button" onclick="location.href='answer.php';" value="Submit Evalution"/></center>  
</form>	
</div>
<div class="col-sm-2"></div>
</div>

</body>

<div class="wrapper row4 bgded overlay" style="background-image:url('../images/demo/backgrounds/003.jpg');">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    
      
  
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Copyright &copy;All Rights Reserved - <a href="/">SurveyFun</a></p>
    <p class="fl_right">Privacy Policy | Terms & Conditions</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>


</html>
 


</html>
