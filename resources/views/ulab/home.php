<!DOCTYPE html>
<html lang="en">
<head>
<title>SurveyFun</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">
  <link rel="stylesheet" href="../css/bootstrap.min.css">



<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="fl_left">
      <ul>
        <li><i class="fa fa-phone"></i> contact number</li>
        <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
      </ul>
    </div>
    <div class="fl_right">
      
        <div class="container">

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                   
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
      <h1><a href="/">SurveyFun</a></h1>
      <p>Survey making website</p>
    </div>
    <div id="quickinfo" class="fl_right">
      <ul class="nospace inline">
        <li><strong>Help line:</strong><br>
          0000000000</li>
        <li><strong>Send mail:</strong><br>
          SurveyFun@gmail.com</li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </header>
  <nav id="mainav" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul class="clear">
      <li class="active"><a href="home">Home</a></li>
      <li><a class="drop">Design</a>
        <ul>
          <li><a href="design">Demo interface</a></li>
          <li><a href="form">Form</a></li>
          <li><a href="survey/view">Level 2</a></li>
          <li><a href="#">Level 3</a></li>
        </ul>
      </li>
      <li><a href="full-width">Your surveys</a>
        <!-- #<ul>
          <li><a href="#">Level 2</a></li>
          <li><a class="drop" href="#">Level 3</a>
            <ul>
              <li><a href="#">Level 3a</a></li>
              <li><a href="#">Level 3b</a></li>
              <li><a href="#">Level 3c</a></li>
            </ul>
          </li>
          <li><a href="#">Level 2</a></li>
        </ul>## -->
      </li>
      <li><a href="#">Analyze</a></li>
      <li><a href="#">Reports</a></li>
      <li><a href="survey/new.php">Evalution</a></li>
      <li><a href="ulab/home.php">Ulab Evalution</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </nav>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<!-- JAVASCRIPTS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>


</html>
 

