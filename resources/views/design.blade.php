<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
<title>SurveyFun | Design | Demo interface</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="../css/demo.css">

</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="fl_left">
      <ul>
        <li><i class="fa fa-phone"></i> contact number</li>
        <li><i class="fa fa-envelope-o"></i> info@surveyFun</li>
      </ul>
    </div>
    <div class="fl_right">
      
        <div class="container">

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                   
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
      <h1><a href="/">SurveyFun</a></h1>
      <p>Survey making website</p>
    </div>
    <div id="quickinfo" class="fl_right">
      <ul class="nospace inline">
        <li><strong>Help line:</strong><br>
          0000000000</li>
        <li><strong>Send mail:</strong><br>
          SurveyFun@gmail.com</li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </header>
  <nav id="mainav" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul class="clear">
      <li class="active"><a href="home">Home</a></li>
      <li><a class="drop">Design</a>
        <ul>
          <li><a href="design">Demo interface</a></li>
          <li><a href="form">Form</a></li>
          <li><a href="#">Level 2</a></li>
          <li><a href="#">Level 3</a></li>
        </ul>
      </li>
      <li><a href="full-width">Your surveys</a>
        <!-- #<ul>
          <li><a href="#">Level 2</a></li>
          <li><a class="drop" href="#">Level 3</a>
            <ul>
              <li><a href="#">Level 3a</a></li>
              <li><a href="#">Level 3b</a></li>
              <li><a href="#">Level 3c</a></li>
            </ul>
          </li>
          <li><a href="#">Level 2</a></li>
        </ul>## -->
      </li>
      <li><a href="#">Analyze</a></li>
      <li><a href="#">Reports</a></li>
      <li><a href="survey/new.php">Evalution</a></li>
      <li><a href="ulab/home.php">Ulab Evalution</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </nav>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('../images/demo/backgrounds/a3.jpg');">
  <section id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul>
      <li><a href="home">Home</a></li>
      <li><a href="about">About</a></li>
      <li><a href="feature">Features</a></li>
      <li><a href="register">Registration</a></li>
      <li><a href="example">Examples</a></li>
      <li><a href="service">Services</a></li>
      <li><a href="#">Help</a></li>
    </ul>
    <!-- ################################################################################################ -->
    <h6 class="heading">Survey Design</h6>
    <!-- ################################################################################################ -->
  </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class= "content">
    
      <!-- ################################################################################################ -->
      <h2>About</h2>
      
      <div class="sdb_holder">
        <div class="one_quarter first">
          <ul>
          <li><a href="#">Solutions</a> </li>
          
            <ul>
              <li><a href="#">Education</a></li>
              <li><a href="#">Government</a></li>
              <li><a href="#">Business</a></li>
              <li><a href="#">Medical</a></li>
              <li><a href="#">Non Profits</a></li>
            </ul>
         

          <li><a href="#">Features</a> </li>
          
            <ul>
              <li><a href="#">Custom Designs</a></li>
              <li><a href="#">Easy to Use</a></li>
              <li><a href="#">Question Types</a></li>
              <li><a href="#">Execute</a></li>
              </ul>
         
                  
          <li><a href="#">Terms of Use</a></li>

          <li><a href="#">Privacy Policy</a></li>

          <li><a href="#">Contact</a></li>
        </ul>
        </div>
      </div>

 <div class="three_quarter"> 
    
      
    <h1>Design your survey</h1>
       <form id="idOfForm"  class="render-wrap"></form>
    <div id="stage1" class="build-wrap"></div>
 
    <button id="edit-form">Edit Form</button>
    <button type="submit">Save</button>
<script >
  // using jQuery
$('#stage1').formRender(
    {
      formData: questionsJSON, // This is data you stored in database when you build the form
      dataType: 'json'
    }
);

$('#idOfForm').on('submit', function (e) {
  e.preventDefault();
// Collect form data
  var formData = $(this).serialize();
// Send data to server
  $.post('save-form.php', formData)
      .done(function (response) {
        console.log('form saved')
      }).fail(function (jqXHR) {
    console.log('form was not saved')
  });
  // Prevent form submission
  return false;
});
</script>
 
    
<div class="clear"></div>
  </main>
</div>
 
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row4 bgded overlay" style="background-image:url('../images/demo/backgrounds/003.jpg');">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    
      
  
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Copyright &copy;All Rights Reserved - <a href="/">SurveyFun</a></p>
    <p class="fl_right">Privacy Policy | Terms & Conditions</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.backtotop.js"></script>
<script src="../js/jquery.mobilemenu.js"></script>
<script src="../js/vendor.js"></script>
<script src="../js/form-builder.min.js"></script>
<script src="../js/form-render.min.js"></script>
<script src="../js/demo.js"></script>

</body>
</html>
 
